#!/usr/bin/env python3

'''
Python program that prints some graphs of scatter plots of z10(f, fprime, z0)
for z0 in [-3,3]x[-3,3]
'''

import pandas as pd
import matplotlib.pyplot as plt


def mk_points(n_points_minus_1, endl, endr):
    '''
    make a range of n_points_minus_1+1 points evenly spaced between endl and
    endr
    '''
    n_points = n_points_minus_1 + 1
    seeds = range(n_points + 1)
    width = endr - endl
    delta = width/n_points
    points = [endl + delta*seed for seed in seeds]
    return points


def complex_cartesian_product(xs, ys):
    '''
    return the set of all pairs x + yi where x in xs, y in ys
    '''
    return [x + y*1j for x in xs for y in ys]


def newton_step(z, f, f_prime):
    '''
    Run a step of Newton's method on z, with f and f_prime

    handles zero division error if f_prime = 0
    '''
    try:
        new_z = z - f(z)/f_prime(z)
        return new_z
    except ZeroDivisionError:
        return z


def newton(z0, f, f_prime, n_steps=10):
    '''
    Run newton's method starting at z0, with functions f and f_prime, with
    n_steps steps
    '''
    z = z0
    for _ in range(n_steps):
        z = newton_step(z, f, f_prime)
    return z


def rim(z):
    '''
    return a tuple of real and imaginary parts of z
    '''
    return (z.real, z.imag)


def f1(z):
    return z - 1
def f1_prime(z):
    return 1


def f2(z):
    return z**2 - 1
def f2_prime(z):
    return 2*z


def f3(z):
    return z**3 - 1
def f3_prime(z):
    return 3*(z**2)


def f4(z):
    return z**4 - 1
def f4_prime(z):
    return 4*(z**3)


def f5(z):
    return z**5 - 1
def f5_prime(z):
    return 5*(z**4)


def f6(z):
    return z**6 - 1
def f6_prime(z):
    return 6*(z**5)


def f7(z):
    return z**7 - 1
def f7_prime(z):
    return 7*(z**6)


def f8(z):
    return z**8 - 1
def f8_prime(z):
    return 8*(z**7)


if __name__ == '__main__':
    xs = mk_points(100, -3, 3)
    ys = mk_points(100, -3, 3)
    zs = complex_cartesian_product(xs,ys)
    funtrips = [
            (1, f1, f1_prime),
            (2, f2, f2_prime),
            (3, f3, f3_prime),
            (4, f4, f4_prime),
            (5, f5, f5_prime),
            (6, f6, f6_prime),
            (7, f7, f7_prime),
            (8, f8, f8_prime)
        ]
    for fun_idx, f, f_prime in funtrips:
        for n_iterations in [0, 1, 2, 4, 8, 16, 32, 64, 128, 256]:
            z10s = [newton(z0, f, f_prime, n_iterations) for z0 in zs]
            rim_z10s = [rim(z10) for z10 in z10s]
            x10s = [x for x, _ in rim_z10s]
            y10s = [y for _, y in rim_z10s]
            filename = 'img_newton_converge_f%d_z%03d.png' % (fun_idx, n_iterations)
            title = 'Scatter plot of $z_{%s}(f_{%s}, z_0)$ for $z_0 \in [-3, 3] \\times [-3, 3]$' % (n_iterations, fun_idx)

            print('%s: %s' % (filename, title))

            plt.title(title)
            plt.scatter(x10s, y10s, s=5, c='green', marker='s', alpha=0.1)
            plt.xlim(-5, 5)
            plt.ylim(-5, 5)
            plt.savefig(filename)
            plt.clf()
