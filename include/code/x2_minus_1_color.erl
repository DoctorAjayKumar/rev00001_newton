#!/usr/bin/env escript

-mode(compile).

main(_) ->
    ok = io:format("x,final_x,color~n"),
    % Want 100 points evenly spaced between -3 and 3
    NumPointsPlus1 = 100000,
    Seeds = lists:seq(0, NumPointsPlus1),
    EndpointLeft = -3,
    EndpointRight = 3,
    Width = EndpointRight - EndpointLeft,
    Delta = Width / NumPointsPlus1,
    Points = [EndpointLeft + Delta*Seed || Seed <- Seeds],
    %ok = io:format("points=~p~n", [Points]),
    ParentPid = self(),
    Pid = spawn_link(fun() -> do_points(Points, ParentPid) end),
    Pid ! start,
    receive
        done -> ok
    after 50000 ->
        ok = io:format("main process timing out~n"),
        erlang:error(timeout)
    end.


-spec do_points(Xs :: [float()], ParentPid :: pid()) -> ok.
do_points(Xs, Parent) ->
    PointSet = gb_sets:from_list(Xs),
    do_point_set(PointSet, Parent).


-spec do_point_set(XSet :: gb_sets:set(), ParentPid :: pid()) -> ok.
do_point_set(XSet, Parent) ->
    case gb_sets:is_empty(XSet) of
        true ->
            %ok = io:format("37~n"),
            Parent ! done,
            ok;
        false ->
            %ok = io:format("41~n"),
            receive
                % run do_point on each X in Xs
                start ->
                    ParentPid = self(),
                    StartX =
                        fun(X) ->
                            Pid = spawn_link(fun() -> do_point(X, ParentPid) end),
                            Pid ! start
                        end,
                    Xs = gb_sets:to_list(XSet),
                    lists:foreach(StartX, Xs),
                    do_point_set(XSet, Parent);

                % X is done, remove it from list
                {done, X} ->
                    %ok = io:format("~p is done~n", [X]),
                    NewXSet = gb_sets:del_element(X, XSet),
                    do_point_set(NewXSet, Parent);

                Unexpected ->
                    ok = io:format("Unexpected message: ~p~n", [Unexpected]),
                    do_point_set(XSet, Parent)

            after 5000 ->
                Xs = gb_sets:to_list(XSet),
                ok = io:format("timed out with state ~p~n", [Xs]),
                erlang:error(timeout)

            end
    end.


-spec do_point(X :: float(), ParentPid :: pid()) -> ok.
do_point(X, ParentPid) ->
    %ok = io:format("ParentPid = ~p", [ParentPid]),
    receive
        start ->
            ok = really_do_point(X),
            ParentPid ! {done, X},
            ok

    after 2000 ->
        ok = io:format("point ~p crashing~n", [X]),
        erlang:error(timeout)
    end.


-spec really_do_point(X0 :: float()) -> ok.
really_do_point(X0) ->
    %ok = io:format("really doing point ~p~n", [X0]),
    NumIterations = 10,
    Folder =
        fun(_Iter, X) ->
            NewX = newton_next(X),
            NewX
        end,
    FinalX = lists:foldl(Folder, X0, lists:seq(1, NumIterations)),
    IsMinus1 = quadrance(-1.0  - FinalX) < 0.1,
    IsPlus1  = quadrance( 1.0  - FinalX) < 0.1,
    Str =
        if
             IsMinus1 ->
                "left";
             IsPlus1 ->
                "right";
             true ->
                "inconclusive"
        end,
    ok = io:format("~f,~f,~s~n", [X0, FinalX, Str]),
    ok.


-spec newton_next(X :: float()) -> float().
newton_next(X) ->
    FX = f(X),
    FPX = f_prime(X),
    case (FPX==0.0) of
        % don't try to divide by zero, just return X
        true ->
            X;
        % otherwise, apply the newton step
        false ->
            NewX = X - FX/FPX,
            NewX
    end.


-spec f(float()) -> float().
f(X) ->
    math:pow(X, 2) - 1.0 .


-spec f_prime(float()) -> float().
f_prime(X) ->
    2*X.

-spec quadrance(float()) -> float().
% just the square for now
quadrance(F) ->
    math:pow(F, 2).


%-spec do_points(Xs :: [float()], ParentPid :: pid()) -> ok.
%% no more points, exit
%do_points([], Parent) ->
%    Parent ! done,
%    ok;
%do_points(Xs, Parent) ->
%    %ok = io:format("line 27, pid=~p~n", [self()]),
%    receive
%        % run do_point on each X in Xs
%        start ->
%            ParentPid = self(),
%            StartX =
%                fun(X) ->
%                    Pid = spawn_link(fun() -> do_point(X, ParentPid) end),
%                    Pid ! start
%                end,
%            lists:foreach(StartX, Xs),
%            do_points(Xs, Parent);
%
%        % X is done, remove it from list
%        {done, X} ->
%            %ok = io:format("~p is done~n", [X]),
%            NewXs = Xs -- [X],
%            do_points(NewXs, Parent);
%
%        Unexpected ->
%            ok = io:format("Unexpected message: ~p~n", [Unexpected]),
%            do_points(Xs, Parent)
%
%    after 5000 ->
%        ok = io:format("timed out with state ~p~n", [Xs]),
%        erlang:error(timeout)
%
%    end.
%
