#!/usr/bin/env python3

'''
Python program that plots some julia sets
'''

import math
import pandas as pd
import matplotlib.pyplot as plt


def mk_points(n_points, endl, endr):
    '''
    make a range of n_points_minus_1+1 points evenly spaced between endl and
    endr
    '''
    n_points_minus1 = n_points - 1
    seeds = range(n_points)
    width = endr - endl
    # -1 here because seeds range from 0..(n_points-1) so for instance, if
    # n_points=10, endl=-3, endr=3, then width=6, n_pointsminus1 = 9, max seed
    # = 9, therefore
    #
    # -3 + 9*(6/9) = -3 + 6 = 3 = endpoint_right
    #
    # so that we get an even spread of points between endpoint_left and
    # endpoint_right
    delta = width/n_points_minus1
    points = [endl + delta*seed for seed in seeds]
    return points


def complex_cartesian_product(xs, ys):
    '''
    return the set of all pairs x + yi where x in xs, y in ys
    '''
    return [x + y*1j for x in xs for y in ys]


def newton_step(z, f, f_prime):
    '''
    Run a step of Newton's method on z, with f and f_prime

    handles zero division error if f_prime = 0
    '''
    try:
        new_z = z - f(z)/f_prime(z)
        return new_z
    except ZeroDivisionError:
        return z


def newton(z0, f, f_prime, n_steps=10):
    '''
    Run newton's method starting at z0, with functions f and f_prime, with
    n_steps steps
    '''
    z = z0
    for _ in range(n_steps):
        z = newton_step(z, f, f_prime)
    return z


def rim(z):
    '''
    return a tuple of real and imaginary parts of z
    '''
    return (z.real, z.imag)


def f_and_fprime(n):
    '''
    Return pair (f_n, f_n prime)
    '''
    def f(z):
        return z**n - 1

    def f_prime(z):
        return n*(z**(n-1))

    return (f, f_prime)


def sinks(n):
    '''
    Return list of sinks for f_n
    '''
    sinks = []
    for iter in range(n):
        theta = 2 * math.pi * (1 / n) * iter
        x = math.cos(theta)
        y = math.sin(theta)
        z = x + y*1j
        sinks.append(z)
    return sinks


def quadrance(z):
    '''
    Return quadrance of a complex number
    '''
    return z.real**2 + z.imag**2


def assign_complex_to_sink(z, sinks):
    '''
    Given a complex number and a list sinks (which should be a list of complex
    numbers)

    - if for some sink s, quadrance(z - s) < 0.01, return ('sunk', idx, s)
      where idx is the 0-index in the list of sinks

    - else, return 'afloat'

    - if there are multiple sinks for which the condition is true, it will
      match the first one in the list
    '''
    for this_idx, this_sink in enumerate(sinks):
        quad = quadrance(z - this_sink)
        if quad < 0.01:
            return ('sunk', this_idx, this_sink)

    return 'afloat'


def assign_color_to_ship_status(ship_status):
    '''
    Given a "ship status" which is either 'afloat' or ('sunk', idx0, _), return
    a name of a color
    '''
    if ship_status == 'afloat':
        # k = black
        return 'k'
    else:
        # Maybe tweak these to make prettier. I chose a palette with a lot of
        # pairwise contrast between the colors, because I hate graphs where you
        # can't tell the difference between different colors. But it's going to
        # be ugly. Oh well
        #
        # List of colors:
        # https://matplotlib.org/3.1.0/gallery/color/named_colors.html
        assert(ship_status[0] == 'sunk')
        colors = {0: 'tab:blue',
                  1: 'tab:orange',
                  2: 'tab:green',
                  3: 'tab:red',
                  4: 'tab:purple',
                  5: 'tab:brown',
                  6: 'tab:pink',
                  7: 'tab:olive'}
        idx0 = ship_status[1]
        return colors[idx0]


def color_of_complex(z, n):
    '''
    assign a color to a complex number, assuming it either falls into one of
    the n sinks. goddamn it you know what's going on. either z is one of the
    zeros of z^n - 1 (or close to it), in which case give it an interesting
    color. otherwise color it black
    '''
    sinx = sinks(n)
    ship_status = assign_complex_to_sink(z, sinx)
    color = assign_color_to_ship_status(ship_status)
    return color


def mk_scatter_df(zs, n, n_steps=256):
    '''
    Return a dataframe with columns x,y,color

    zs: complex points
    n: polynomial degree for f(z) = z^n - 1
    n_steps: number of steps in Newton's method
    '''
    f, fprime = f_and_fprime(n)
    rows = []
    for this_z0 in zs:
        this_z256 = newton(this_z0, f, fprime, n_steps=n_steps)
        this_x, this_y = rim(this_z0)
        this_color = color_of_complex(this_z256, n)
        this_row = {'x': this_x,
                    'y': this_y,
                    'color': this_color}
        rows.append(this_row)
    df = pd.DataFrame.from_records(rows)
    return df


def mk_scatter(n, n_xpoints=100, n_ypoints=100, xmin=-3, xmax=3, ymin=-3, ymax=3):
    '''
    make a scatter plot for the degree n polynomial, display it or save it or
    neither depending on which line I commented out

    n: degree of the polynomial z^n - 1
    n_xpoints: number of points on x direction
    n_ypoints: number of points on y direction
    xmin: min x value
    xmax: max x value
    ymin: min y value
    ymax: max y value
    '''
    xs = mk_points(n_xpoints, xmin, xmax)
    ys = mk_points(n_ypoints, ymin, ymax)
    zs = complex_cartesian_product(xs, ys)
    df = mk_scatter_df(zs, n)
    color_column = df['color']
    unique_colors = color_column.unique()
    for this_color in unique_colors:
        rows_of_this_color = df[df['color'] == this_color]
        this_color_xs = rows_of_this_color['x']
        this_color_ys = rows_of_this_color['y']

        # might want to toy with the parameters here to change the graph
        # https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.scatter.html
        plt.scatter(this_color_xs, this_color_ys,
                    s=0.1,
                    c=this_color,
                    marker='.',
                    alpha=1)
        plt.xlim(xmin-1, xmax+1)
        plt.ylim(ymin-1, ymax+1)

    title = 'Julia set of $z^{%s} - 1$' % (n)
    plt.title(title)

    # Uncomment this if you want to save the files
    #filename = 'img_julia_z%d.png' % (n)
    #plt.savefig(filename)

    plt.show()
    plt.clf()


if __name__ == '__main__':
    # Probably want to lower this if you are experimenting so it's faster
    point_density = 800
    for n in [3,4,5,6,7,8]:
        mk_scatter(n, n_xpoints=point_density, n_ypoints=point_density)
