#!/usr/bin/env python3

'''
Python program that does same thing as x2_minus_1_color.erl, for benchmark
comparison
'''

import pandas as pd
import matplotlib.pyplot as plt

def f(x):
    return x**2 - 1


def f_prime(x):
    return 2*x


def newton_next(x):
    # avoid divide by 0
    if f_prime(x) == 0.0:
        return x
    else:
        return x - f(x)/f_prime(x)


def quadrance(x):
    return x**2


def newton(x0, dicts):
    n_iterations = 10
    x = x0
    for _ in range(n_iterations):
        x = newton_next(x)
    is_minus_one = quadrance(-1 - x) < 0.1
    is_plus_one  = quadrance( 1 - x) < 0.1
    color = None
    if is_minus_one:
        color = 'black'
    elif is_plus_one:
        color = 'pink'
    else:
        color = 'green'

    this_dict = {
            'x': x0,
            'y': 0.0,
            'final_x': x,
            'color': color
        }
    dicts.append(this_dict)
    return dicts

if __name__ == '__main__':
    #n_points = 10
    n_points = 1000
    seeds = range(n_points + 1)
    endpoint_left = -3
    endpoint_right = 3
    width = endpoint_right - endpoint_left
    delta = width / (n_points)
    points = [endpoint_left + delta*seed for seed in seeds]

    dicts = []
    for point in points:
        dicts = newton(point, dicts)
    df = pd.DataFrame.from_records(dicts)

    for this_color in ['pink', 'black', 'green']:
        this_color_points = df[df['color'] == this_color]
        xs = this_color_points['x']
        ys = this_color_points['y']
        plt.scatter(xs, ys, c=this_color)

    plt.savefig('img_x2_minus_1_color.png')
    #plt.show()
